/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tunisiamall;

import gui.CarteList;
import gui.CatalogueList;
import gui.EnseigneList;
import gui.PromotionList;
import gui.SplashScreen;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author bof
 */
public class MyCanvas2 extends Canvas implements CommandListener{
 int x = getWidth();
    int y = getHeight();
    int posX ;
    int posY ;
     Image imgInterface;
      Image img;
      Command cmdCarte ;
      public MyCanvas2(){
          posX=x/10;
          posY=y/6-2;
          cmdCarte= new Command("Carte", Command.OK, 0);
          
          addCommand(cmdCarte);
          setCommandListener(this);
          try{
            
              imgInterface =Image.createImage("/tunisiamall/interface2.png");
              img=imgInterface;
              System.out.println("width : "+x+" height : "+y);
          }catch(Exception e){
              System.out.println("erreur image "+e);
          }
      }
    protected void paint(Graphics g) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   g.drawImage(img, 0, 0,  Graphics.TOP | Graphics.LEFT);
        g.setColor(255, 255, 255);
        g.drawRect(posX, posY, x/3+2, y/3);
    }
    protected void keyPressed(int keyCode) {
     int gameAction = getGameAction(keyCode);
     if(gameAction == UP ){
         if(posY==y/6-2+y/3+8){
             posY=posY-y/3-8;
         }
     }
     
     if(gameAction == FIRE){
         
         if(posY==y/6-2+y/3+8 && posX==x/10){
             Midlet.INSTANCE.display.setCurrent(new EnseigneList());
         }
         if(posY==y/6-2 && posX==x/10){
              Midlet.INSTANCE.display.setCurrent(new CatalogueList());
         }
         if(posY==y/6-2 && posX==x/10+x/2-4){
              Midlet.INSTANCE.display.setCurrent(new PromotionList());
         }
     }
     if(gameAction == DOWN ){
         if(posY==y/6-2){
             posY=posY+y/3+8;
         }
        
     }
     if(gameAction == RIGHT ){
      
    
           if(posX==x/10)
           posX=posX+x/2-4;
       
        
     }
     if(gameAction == LEFT ){
           
            if(posX>x/2+4)
           posX=posX-x/2+4;
                   
         
         
     }
         repaint();
     
    }

    public void commandAction(Command c, Displayable d) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      if(c==cmdCarte){
          Midlet.INSTANCE.display.setCurrent(new CarteList());
          
      }
    }
    
}
