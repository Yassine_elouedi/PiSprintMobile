/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tunisiamall.entities;

import java.util.Date;


/**
 *
 * @author Sami
 */
public class Catalogue {

    private int id;
    private Date DateDebut;
    private Date DateFin;
    private String Ref;
    private String Type;
    private String slogan;
    private String descriptif;
    private int id_ensigne;
    private String libelleEnseigne;
    private int User_id;
    private String Active;
   
public Catalogue(){}
    public Catalogue(int id, Date DateDebut, Date DateFin, String Ref, String Type, String slogan, String descriptif, String Active) {
        this.id = id;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Ref = Ref;
        this.Type = Type;
        this.slogan = slogan;
        this.descriptif = descriptif;
        this.Active = Active;
    }

    public Catalogue(Date DateDebut, Date DateFin, String Ref, String Type, String slogan, String descriptif, String Active) {
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Ref = Ref;
        this.Type = Type;
        this.slogan = slogan;
        this.descriptif = descriptif;
        this.Active = Active;
    }

    public Catalogue(int id, Date DateDebut, Date DateFin, String Ref, String slogan, String descriptif) {
        this.id = id;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Ref = Ref;
        this.slogan = slogan;
        this.descriptif = descriptif;
        
    }

    public Catalogue(Date DateDebut, Date DateFin, String Ref, String Type, String slogan, String descriptif, int id) {
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Ref = Ref;
        this.Type = Type;
        this.slogan = slogan;
        this.descriptif = descriptif;
        this.Active = Active;
        
    }

    public Catalogue(int id, Date DateDebut, Date DateFin, String Ref, String Type, String slogan, String descriptif, String libelleEnseigne,String Active) {
        this.id = id;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Ref = Ref;
        this.Type = Type;
        this.slogan = slogan;
        this.descriptif = descriptif;
        this.libelleEnseigne = libelleEnseigne;
        this.Active=Active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateDebut() {
        return DateDebut;
    }

    public void setDateDebut(Date DateDebut) {
        this.DateDebut = DateDebut;
    }

    public Date getDateFin() {
        return DateFin;
    }

    public void setDateFin(Date DateFin) {
        this.DateFin = DateFin;
    }

    public String getRef() {
        return Ref;
    }

    public void setRef(String Ref) {
        this.Ref = Ref;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    public int getId_ensigne() {
        return id_ensigne;
    }

    public void setId_ensigne(int id_ensigne) {
        this.id_ensigne = id_ensigne;
    }

    public int getUser_id() {
        return User_id;
    }

    public void setUser_id(int User_id) {
        this.User_id = User_id;
    }

    public String getLibelleEnseigne() {
        return libelleEnseigne;
    }

    public void setLibelleEnseigne(String libelleEnseigne) {
        this.libelleEnseigne = libelleEnseigne;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String Active) {
        this.Active = Active;
    }
}
