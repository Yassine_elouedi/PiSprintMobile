/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Yassine
 */
public class Carte {
    private int id ; 
    private int numero ; 
    private int nbrPt ; 
    private Enseigne enseigne ; 
    private User client ; 

    public Carte(int numero, int nbrPt, Enseigne enseigne, User client) {
        this.numero = numero;
        this.nbrPt = nbrPt;
        this.enseigne = enseigne;
        this.client = client;
    }

    public Carte() {
    }
   
    public int getNumero() {
        return numero;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNbrPt() {
        return nbrPt;
    }

    public void setNbrPt(int nbrPt) {
        this.nbrPt = nbrPt;
    }

    

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }
    
    
}
