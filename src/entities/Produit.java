/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

//import Esprit.Tunisiamall.Util.Enum.Categories;
//import java.awt.Image;
import java.io.InputStream;

/**
 *
 * @author ISLEM
 */
public class Produit {
    
    
    private int id;
    private String Reference;
    private String Libelle;
    private String Descriptif;
    private String Prix;
    private String Categorie;
    private InputStream Image;
    private int NbPoint;
    //private panier panier;

    public Produit() {
       
    }

    public Produit(int id, String Reference, String Libelle, String Descriptif, String Prix, String Categorie, InputStream Image, int NbPoint) {
        this.id = id;
        this.Reference = Reference;
        this.Libelle = Libelle;
        this.Descriptif = Descriptif;
        this.Prix = Prix;
        this.Categorie = Categorie;
        this.Image = Image;
        this.NbPoint = NbPoint;
        
    }

    public Produit(String Reference, String Libelle, String Descriptif, String Prix, String Categorie, InputStream Image, int NbPoint) {
        this();
        this.Reference = Reference;
        this.Libelle = Libelle;
        this.Descriptif = Descriptif;
        this.Prix = Prix;
        this.Categorie = Categorie;
        this.Image = Image;
        this.NbPoint = NbPoint;
       
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String Reference) {
        this.Reference = Reference;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String Libelle) {
        this.Libelle = Libelle;
    }

    public String getDescriptif() {
        return Descriptif;
    }

    public void setDescriptif(String Descriptif) {
        this.Descriptif = Descriptif;
    }

    public String getPrix() {
        return Prix;
    }

    public void setPrix(String Prix) {
        this.Prix = Prix;
    }

    public String getCategorie() {
        return Categorie;
    }

    public void setCategorie(String Categorie) {
        this.Categorie = Categorie;
    }

    public InputStream getImage() {
        return Image;
    }

    public void setImage(InputStream Image) {
        this.Image = Image;
    }

    public int getNbPoint() {
        return NbPoint;
    }

    public void setNbPoint(int NbPoint) {
        this.NbPoint = NbPoint;
    }
  
    
}

   