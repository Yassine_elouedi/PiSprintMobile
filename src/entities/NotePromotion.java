/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Asma
 */
public class NotePromotion {
    
    
    private int id_user;
    private int id_promotion;
    private int note;

    public NotePromotion(int id_user, int id_promotion, int note) {
        this.id_user = id_user;
        this.id_promotion = id_promotion;
        this.note = note;
    }

    public NotePromotion() {
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_promotion() {
        return id_promotion;
    }

    public void setId_promotion(int id_promotion) {
        this.id_promotion = id_promotion;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }
    
    
    
}
