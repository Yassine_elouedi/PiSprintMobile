/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Asus X550L
 */
public class Promotion {
    private int id;
    private String DateDebut;
    private String DateFin;
    private int Tauxdereduction;
    private String Image;
    private String Description;
    private String etat;
    private Integer note;
    private User User;

    public Promotion() {
    }

    public Promotion(int id, String DateDebut, String DateFin, int Tauxdereduction, String Image, String Description, String etat) {
        this.id = id;
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Tauxdereduction = Tauxdereduction;
        this.Image = Image;
        this.Description = Description;
        this.etat = etat;
    }

    public Promotion(String DateDebut, String DateFin, int Tauxdereduction, String Image, String Description, String etat) {
        this.DateDebut = DateDebut;
        this.DateFin = DateFin;
        this.Tauxdereduction = Tauxdereduction;
        this.Image = Image;
        this.Description = Description;
        this.etat = etat;
    }

   


    public Promotion(String Description) {
        this.Description = Description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateDebut() {
        return DateDebut;
    }

    public void setDateDebut(String DateDebut) {
        this.DateDebut = DateDebut;
    }

    public String getDateFin() {
        return DateFin;
    }

    public void setDateFin(String DateFin) {
        this.DateFin = DateFin;
    }

    public int getTauxdereduction() {
        return Tauxdereduction;
    }

    public void setTauxdereduction(int Tauxdereduction) {
        this.Tauxdereduction = Tauxdereduction;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User User) {
        this.User = User;
    }


  
    
}
