/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.InputStream;


/**
 *
 * @author Yassine
 */
public class Enseigne {
   
    private int id;
    private String Libelle;
    private int Etage;
    private String Description;
    private String Categorie;
    private  String TimeOuverture;
    private String TimeFermeture;
    private String URL;
    private String Image;
    private int NBJour;
    private int User_id;

    public Enseigne() {
        
    }
    public Enseigne(int id, String Libelle) {
        this.id = id;
        this.Libelle = Libelle;
    }

     public Enseigne(String Libelle, int Etage, String Categorie, String Image , int User_id) {
        this();
        this.Libelle = Libelle;
        this.Etage = Etage;
        this.Categorie = Categorie;
        this.Image = Image;
        this.User_id=User_id ;
        
    }

    public Enseigne(String Libelle, int Etage, String Categorie, String TimeOuverture, String TimeFermeture, String URL, String Image, int NBJour, int User_id) {
        this.Libelle = Libelle;
        this.Etage = Etage;
        this.Categorie = Categorie;
        this.TimeOuverture = TimeOuverture;
        this.TimeFermeture = TimeFermeture;
        this.URL = URL;
        this.Image = Image;
        this.NBJour = NBJour;
        this.User_id = User_id;
    }
    public Enseigne(int id,String Libelle,String Description, int Etage, String URL, int NBJour, int User_id) {
        this.id=id;
        this.Libelle = Libelle;
        this.Etage = Etage;
        this.Description=Description;
        this.URL = URL;
   
        this.NBJour = NBJour;
        this.User_id = User_id;
    }
    public Enseigne(String Libelle,String Description, int Etage, String Categorie, String URL, String Image, int NBJour, int User_id) {
        this.Libelle = Libelle;
        this.Description=Description;
        this.Etage = Etage;
        this.Categorie = Categorie;
        this.URL = URL;
        this.Image = Image;
        this.NBJour = NBJour;
        this.User_id = User_id;
    }
    
    public Enseigne(int id, String Libelle, int Etage, String Categorie, String Image) {
        this.id = id;
        this.Libelle = Libelle;
        this.Etage = Etage;
        this.Categorie = Categorie;
        this.Image = Image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String Libelle) {
        this.Libelle = Libelle;
    }

    public int getEtage() {
        return Etage;
    }

    public void setEtage(int Etage) {
        this.Etage = Etage;
    }

    public String getCategorie() {
        return Categorie;
    }

    public void setCategorie(String Categorie) {
        this.Categorie = Categorie;
    }

    public String getTimeOuverture() {
        return TimeOuverture;
    }

    public void setTimeOuverture(String TimeOuverture) {
        this.TimeOuverture = TimeOuverture;
    }

    public String getTimeFermeture() {
        return TimeFermeture;
    }

    public void setTimeFermeture(String TimeFermeture) {
        this.TimeFermeture = TimeFermeture;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public int getNBJour() {
        return NBJour;
    }

    public void setNBJour(int NBJour) {
        this.NBJour = NBJour;
    }

    public int getUser_id() {
        return User_id;
    }

    public void setUser_id(int User_id) {
        this.User_id = User_id;
    }

    

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
    
    
}
