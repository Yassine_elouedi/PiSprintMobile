/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tunisiamall.handler;


import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import tunisiamall.entities.Catalogue;



/**
 *
 * @author Yassine
 */
public class CatalogueHandler extends DefaultHandler{

    private Vector  CatalogueVector;

    public CatalogueHandler() {
        CatalogueVector = new Vector();
    }

    public Catalogue[] getCatalogue() {
         Catalogue[] CatalogueTab = new Catalogue[ CatalogueVector.size()];
       CatalogueVector.copyInto( CatalogueTab);
        return CatalogueTab;
    }

    String selectedBalise = "";
    Catalogue selectedCatalogue;
    

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Catalogue")) {
            selectedCatalogue = new  Catalogue();
           
           
        } else if (qName.equals("IdCatalogue")) {
            selectedBalise = "IdCatalogue";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "Libelle";}
         else if (qName.equals("Slogan")) {
            selectedBalise = "Slogan";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Catalogue")) {
            CatalogueVector.addElement(selectedCatalogue);
            selectedCatalogue = null;
           
        } else if (qName.equals("IdCatalogue")) {
            selectedBalise = "";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "";
        }
          else if (qName.equals("Slogan")) {
            selectedBalise = "";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (selectedCatalogue != null) {
            if (selectedBalise.equals("IdCatalogue")) {
                selectedCatalogue.setId(Integer.parseInt(new String(chars, i, i1)));
            
            }
            if (selectedBalise.equals("Libelle")) {
                selectedCatalogue.setLibelleEnseigne(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("Slogan")) {
                selectedCatalogue.setSlogan(new String(chars, i, i1));
              
            }
//            if (selectedBalise.equals("Etage")) {
//                selectedCatalogue.setEtage(Integer.parseInt(new String(chars, i, i1)));
//              
//            }
//            if (selectedBalise.equals("Image")) {
//                selectedCatalogue.setImage(new String(chars, i, i1));
//               
//            }
//            if (selectedBalise.equals("Categorie")) {
//                selectedCatalogue.setCategorie(new String(chars, i, i1));
//              
//            }
//            if (selectedBalise.equals("TimeOuverture")) {
//                selectedCatalogue.setTimeOuverture(new String(chars, i, i1));
//             
//            }
//              if (selectedBalise.equals("TimeFermeture")) {
//                selectedCatalogue.setTimeFermeture(new String(chars, i, i1));
//             
//            }
//                if (selectedBalise.equals("URL")) {
//                selectedCatalogue.setURL(new String(chars, i, i1));
//             
//            }
//                  if (selectedBalise.equals("NBJour")) {
//                selectedCatalogue.setNBJour(Integer.parseInt( new String(chars, i, i1)));
             
            }
                    
        }
    }

    

