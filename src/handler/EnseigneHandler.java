/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import entities.Enseigne;


/**
 *
 * @author Yassine
 */
public class EnseigneHandler extends DefaultHandler{

    private Vector  EnseigneVector;

    public EnseigneHandler() {
         EnseigneVector = new Vector();
    }

    public Enseigne[] getEnseigne() {
         Enseigne[] EnseigneTab = new  Enseigne[ EnseigneVector.size()];
        EnseigneVector.copyInto( EnseigneTab);
        return  EnseigneTab;
    }

    String selectedBalise = "";
     Enseigne selectedEnseigne;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Enseigne")) {
            selectedEnseigne = new  Enseigne();
           
        } else if (qName.equals("IdEnseigne")) {
            selectedBalise = "IdEnseigne";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "Libelle";
        } else if (qName.equals("Description")) {
            selectedBalise = "Description";
        } else if (qName.equals("Etage")) {
            selectedBalise = "Etage";
        } else if (qName.equals("Image")) {
            selectedBalise = "Image";
        } else if (qName.equals("Categorie")) {
            selectedBalise = "Categorie";
        } else if (qName.equals("TimeOuverture")) {
            selectedBalise = "TimeOuverture";
        }
        else if (qName.equals("TimeFermeture")) {
            selectedBalise = "TimeFermeture";
        }
        else if (qName.equals("URL")) {
            selectedBalise = "URL";
        }
        else if (qName.equals("NBJour")) {
            selectedBalise = "NBJour";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Enseigne")) {
            EnseigneVector.addElement(selectedEnseigne);
            selectedEnseigne = null;
           
        } else if (qName.equals("IdEnseigne")) {
            selectedBalise = "";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "";
        } else if (qName.equals("Description")) {
            selectedBalise = "";
        } else if (qName.equals("Etage")) {
            selectedBalise = "";
        } else if (qName.equals("Image")) {
            selectedBalise = "";
        } else if (qName.equals("Categorie")) {
            selectedBalise = "";
        } else if (qName.equals("TimeOuverture")) {
            selectedBalise = "";
        }
        else if (qName.equals("TimeFermeture")) {
            selectedBalise = "";
        }
        else if (qName.equals("URL")) {
            selectedBalise = "";
        }
        else if (qName.equals("NBJour")) {
            selectedBalise = "";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (selectedEnseigne != null) {
            if (selectedBalise.equals("IdEnseigne")) {
                selectedEnseigne.setId(Integer.parseInt(new String(chars, i, i1)));
            
            }
            if (selectedBalise.equals("Libelle")) {
                selectedEnseigne.setLibelle(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("Description")) {
                selectedEnseigne.setDescription(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("Etage")) {
                selectedEnseigne.setEtage(Integer.parseInt(new String(chars, i, i1)));
              
            }
            if (selectedBalise.equals("Image")) {
                selectedEnseigne.setImage(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("Categorie")) {
                selectedEnseigne.setCategorie(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("TimeOuverture")) {
                selectedEnseigne.setTimeOuverture(new String(chars, i, i1));
             
            }
              if (selectedBalise.equals("TimeFermeture")) {
                selectedEnseigne.setTimeFermeture(new String(chars, i, i1));
             
            }
                if (selectedBalise.equals("URL")) {
                selectedEnseigne.setURL(new String(chars, i, i1));
             
            }
                  if (selectedBalise.equals("NBJour")) {
                selectedEnseigne.setNBJour(Integer.parseInt( new String(chars, i, i1)));
             
            }
                    
        }
    }
}
    

