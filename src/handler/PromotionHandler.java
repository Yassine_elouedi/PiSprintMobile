/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import entities.Promotion;

/**
 *
 * @author Asma
 */
public class PromotionHandler extends DefaultHandler{

    private Vector promotionVector;

    public PromotionHandler() {
        promotionVector = new Vector();
    }

    public Promotion[] getPromotion() {
        Promotion[] promotionTab = new Promotion[promotionVector.size()];
        promotionVector.copyInto(promotionTab);
        return promotionTab;
    }

    String selectedBalise = "";
    Promotion selectedPromotion;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Promotion")) {
            selectedPromotion = new Promotion();
           
        } else if (qName.equals("IdPromotion")) {
            selectedBalise = "IdPromotion";
        } else if (qName.equals("DateDebut")) {
            selectedBalise = "DateDebut";
        } else if (qName.equals("DateFin")) {
            selectedBalise = "DateFin";
        } else if (qName.equals("Tauxdereduction")) {
            selectedBalise = "Tauxdereduction";
        } else if (qName.equals("Image")) {
            selectedBalise = "Image";
        } else if (qName.equals("Description")) {
            selectedBalise = "Description";
        } else if (qName.equals("note")) {
            selectedBalise = "note";
        }
        
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Promotion")) {

            promotionVector.addElement(selectedPromotion);
            selectedPromotion = null;
        } else if (qName.equals("IdPromotion")) {
            selectedBalise = "";
        } else if (qName.equals("DateDebut")) {
            selectedBalise = "";
        } else if (qName.equals("DateFin")) {
            selectedBalise = "";
        } else if (qName.equals("Tauxdereduction")) {
            selectedBalise = "";
        } else if (qName.equals("Image")) {
            selectedBalise = "";
        } else if (qName.equals("Description")) {
            selectedBalise = "";
         } else if (qName.equals("note")) {
            selectedBalise = "";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (selectedPromotion != null) {
            if (selectedBalise.equals("IdPromotion")) {
                selectedPromotion.setId(Integer.parseInt(new String(chars, i, i1)));
            
            }
            if (selectedBalise.equals("DateDebut")) {
                selectedPromotion.setDateDebut(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("DateFin")) {
                selectedPromotion.setDateDebut(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("Tauxdereduction")) {
                selectedPromotion.setTauxdereduction(Integer.parseInt(new String(chars, i, i1)));
              
            }
            if (selectedBalise.equals("Image")) {
                selectedPromotion.setImage(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("Description")) {
                selectedPromotion.setDescription(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("note")) {
                selectedPromotion.setNote(Integer.valueOf(new String(chars, i, i1)));
             
            }
        }
    }
}
    

