    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import entities.NotePromotion;

/**
 *
 * @author Asma
 */
public class NoteHandler
{
     private Vector notes;
    boolean id_user=false;
    boolean  id_promotion=false;
    boolean note=false;
     public NoteHandler() 
    {
       notes=new Vector();
    }
    private NotePromotion currenteval;
    public NotePromotion [] getProduit()
    {
        NotePromotion[] e=new NotePromotion[notes.size()];
        notes.copyInto(e);
        return e;
        
    }
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("notepromotion")) {
            currenteval = new NotePromotion();
            currenteval.setId_user(Integer.parseInt(attributes.getValue("id_user")));
            currenteval.setId_promotion(Integer.parseInt(attributes.getValue("id_promotion")));
            currenteval.setNote(Integer.parseInt(attributes.getValue("note")));
            
           
        } else if (qName.equals("id_user")) {
            id_user = true;   
        } else if (qName.equals("id_promotion")) {
            id_promotion = true;   
        } else if (qName.equals("note")) {
            note = true;
        
        }
        
    }
    
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("notepromotion")) 
        {
            notes.addElement(currenteval);
            currenteval= null;
        } 
        else if (qName.equals("id_user")) 
        {
            id_user = false;
        } 
        else if (qName.equals("id_promotion")) 
        {
            id_promotion = false;
        } 
        else if (qName.equals("note")) 
        {
            note = false;
        }
       
    }
    
    
    public void characters(char[] ch, int start, int length) throws SAXException {
     if (currenteval != null) {
            if (id_user == true) {
                String iduser= new String(ch, start, length).trim();
                currenteval.setId_user(Integer.parseInt(iduser));
            }
             else if (id_promotion == true) {
                String idproduitE = new String(ch, start, length).trim();
                currenteval.setId_promotion(Integer.parseInt(idproduitE));
            }
            else if (note == true) {
                String note = new String(ch, start, length).trim();
                currenteval.setNote(Integer.parseInt(note));
            }
           
        }
        
        
    }
}
