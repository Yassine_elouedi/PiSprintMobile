/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import entities.Carte;


/**
 *
 * @author Yassine
 */
public class CarteHandler extends DefaultHandler{

    private Vector  CarteVector;

    public CarteHandler() {
         CarteVector = new Vector();
    }

    public Carte[] getCarte() {
         Carte[] CarteTab = new  Carte[ CarteVector.size()];
        CarteVector.copyInto( CarteTab);
        return  CarteTab;
    }

    String selectedBalise = "";
     Carte selectedCarte;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Carte")) {
            selectedCarte = new  Carte();
           
        } else if (qName.equals("IdCarte")) {
            selectedBalise = "IdCarte";
        } else if (qName.equals("Numero")) {
            selectedBalise = "Numero";
        } else if (qName.equals("NbrPt")) {
            selectedBalise = "NbrPt";
   
    }}

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Carte")) {
            CarteVector.addElement(selectedCarte);
            selectedCarte = null;
           
        } else if (qName.equals("IdCarte")) {
            selectedBalise = "";
        } else if (qName.equals("Numero")) {
            selectedBalise = "";
        } else if (qName.equals("NbrPt")) {
            selectedBalise = "";
        }}

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (selectedCarte != null) {
            if (selectedBalise.equals("IdCarte")) {
                selectedCarte.setId(Integer.parseInt(new String(chars, i, i1)));
            
            }
            if (selectedBalise.equals("Numero")) {
                selectedCarte.setNumero(Integer.parseInt(new String(chars, i, i1)));
               
            }
            if (selectedBalise.equals("NbrPt")) {
                selectedCarte.setNbrPt(Integer.parseInt(new String(chars, i, i1)));
              
            }

                    
        }
    }
}
    

