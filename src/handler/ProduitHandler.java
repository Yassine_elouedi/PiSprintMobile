/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import entities.Produit;

/**
 *
 * @author ISLEM
 */
public class ProduitHandler extends DefaultHandler {
    
    private Vector ProduitVector;

    public ProduitHandler() {
         ProduitVector = new Vector();
    }

    public Produit[] getProduit() {
        Produit[] ProduitTab = new  Produit[ ProduitVector.size()];
        ProduitVector.copyInto( ProduitTab);
        return  ProduitTab;
    }

    String selectedBalise = "";
    Produit selectedProduit;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Produit")) {
            selectedProduit = new  Produit();
           
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("Reference")) {
            selectedBalise = "Reference";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "Libelle";
        } else if (qName.equals("Descriptif")) {
            selectedBalise = "Descriptif";
        } else if (qName.equals("Prix")) {
            selectedBalise = "Prix";
        } else if (qName.equals("Categorie")) {
            selectedBalise = "Categorie";
        } else if (qName.equals("Image")) {
            selectedBalise = "Image";
        }
        else if (qName.equals("NbPoint")) {
            selectedBalise = "NbPoint";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Produit")) {
            ProduitVector.addElement(selectedProduit);
            selectedProduit = null;
           
        } else if (qName.equals("id")) {
            selectedBalise = "";
        } else if (qName.equals("Reference")) {
            selectedBalise = "";
        } else if (qName.equals("Libelle")) {
            selectedBalise = "";
        } else if (qName.equals("Descriptif")) {
            selectedBalise = "";
        } else if (qName.equals("Prix")) {
            selectedBalise = "";
        } else if (qName.equals("Categorie")) {
            selectedBalise = "";
        } else if (qName.equals("Image")) {
            selectedBalise = "";
        }
        else if (qName.equals("NbPoint")) {
            selectedBalise = "";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (selectedProduit != null) {
            if (selectedBalise.equals("id")) {
                selectedProduit.setId(Integer.parseInt(new String(chars, i, i1)));
            
            }
            if (selectedBalise.equals("Reference")) {
                selectedProduit.setLibelle(new String(chars, i, i1));
               
            }
            if (selectedBalise.equals("Libelle")) {
                selectedProduit.setLibelle(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("Descriptif")) {
                selectedProduit.setDescriptif(new String(chars, i, i1));
              
            }
            if (selectedBalise.equals("Prix")) {
//                selectedProduit.setPrix(Float.parseFloat(new String(chars, i, i1)));
               
            }
            if (selectedBalise.equals("Categorie")) {
                selectedProduit.setCategorie(new String(chars, i, i1));
              
            }
           
             
            }
              if (selectedBalise.equals("NbPoint")) {
                selectedProduit.setNbPoint(Integer.parseInt(new String(chars, i, i1)));
             
            }
                    
        }
    }

