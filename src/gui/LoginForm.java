/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dao.UserDAO;
import entities.User;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import tunisiamall.Midlet;
import tunisiamall.MyCanvas;
import tunisiamall.MyCanvas2;

/**
 *
 * @author bof
 */
public class LoginForm extends Form implements CommandListener, Runnable {
   
    TextField tLog = new TextField("login","",12, TextField.ANY);
    TextField tPassword = new TextField("password","",12,TextField.PASSWORD);
    
    Command cmdLogin = new Command("Login", Command.OK, 0);
    Command cmdInscription = new Command("Inscription",Command.OK,1);
    Command cmdBack = new Command("Retour",Command.BACK,0);
    

    public LoginForm() {
        super("login");
        append(tLog);
        append(tPassword);
        addCommand(cmdLogin);
        addCommand(cmdInscription);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     if(c==cmdLogin){
              
                Thread th = new Thread(this);
            th.start();
            }
            if(c==cmdInscription){
             
                Midlet.INSTANCE.display.setCurrent(new InscriptionForm());
            }
            if(c==cmdBack){
                Midlet.INSTANCE.display.setCurrent(new MyCanvas());
            }
    }
    

    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   String log = tLog.getString();
        String pass = tPassword.getString();
        
        boolean result = new UserDAO().login(new User(log, pass));
        Alert alert = new Alert("R�sultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("login r�ussi");
            Midlet.INSTANCE.display.setCurrent(alert);
             Midlet.INSTANCE.display.setCurrent(new MyCanvas2());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Erreur ");
            Midlet.INSTANCE.display.setCurrent(alert);
    
    }
    }
    
}
