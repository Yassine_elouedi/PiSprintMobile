/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.StringItem;
import entities.Produit;
import tunisiamall.Midlet;

/**
 *
 * @author ISLEM
 */
public class DetailProduitForm extends Form implements CommandListener {

    public static Produit p;
    String url;
    StringBuffer stb;
    Image img;
    Image stars;
    int ch;
 
    StringItem Reference = new StringItem("Reference :","");
    StringItem libelle = new StringItem("Libelle:","");
    StringItem Description = new StringItem("Description:","");
   
    Command Retour= new Command("Retour", Command.SCREEN, 0);
    
    
    public DetailProduitForm(String title) {
        super("Detail produit");
        this.p = p;
        
        Reference.setText(String.valueOf(p.getReference()));
        append(Reference);
        libelle.setText(String.valueOf(p.getLibelle()));
        append(libelle);
        Description.setText(String.valueOf(p.getDescriptif()));
        append(Description);
        
        addCommand(Retour);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if(c==Retour)
        {
            Midlet.INSTANCE.display.setCurrent(new ProduitList());
        }
    }
    
}
