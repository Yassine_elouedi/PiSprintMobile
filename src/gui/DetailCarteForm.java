/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.StringItem;
import entities.Carte;
import tunisiamall.Midlet;


/**
 *
 * @author Yassine
 */
public class DetailCarteForm extends Form implements CommandListener{
    public static Carte p;
    String url;
    StringBuffer stb;
    Image img;
    Image stars;
    int ch;
 
    StringItem Numero = new StringItem("Numero", "");
    StringItem nbrpt = new StringItem("Nombre de points :","");
    

    Command Noter = new Command("Noter carte", Command.SCREEN, 0);
    Command Retour= new Command("Retour", Command.SCREEN, 0);

    public DetailCarteForm(Carte p) {
        
         super("Detail carte");
        this.p = p;
//              if(!"".equals(p.getImage()))
//        {
//            url = p.getImage();
//            try {
//                HttpConnection hc = (HttpConnection)Connector.open(url);
//                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
//                img = Image.createImage(dis);
//            } catch (IOException ex) {
//                 System.out.println(ex.getMessage());
//            }
//            append(img);
//        }
         
        
        Numero.setText(String.valueOf(p.getNumero()));
        append(Numero);
        nbrpt.setText(String.valueOf(p.getNbrPt()));
        append(nbrpt);
       
//        if(!"".equals(p.getNote()));
//        {
//            try {
//                int n=p.getNote().intValue();
//           
//                int x=n/4;
//                HttpConnection hc = (HttpConnection)Connector.open("http://localhost/TunisiaMall22/Stars/"+String.valueOf(n)+"s.bmp");
//                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
//                stars = Image.createImage(dis);
//                append(stars);
//            } catch (Exception ex) {
//                 System.out.println(ex.getMessage());
//            }
//        }
       
        //image.setText(String.valueOf(p.getImage()));
        //append(image);
       // etat.setText(String.valueOf(p.getEtat()));
        //append(etat);
        //eval.setText(String.valueOf(p.getEval()));
        //append(eval);
        addCommand(Retour);
        addCommand(Noter);
        setCommandListener(this);
    }
   

    public void commandAction(Command c, Displayable d)
    {
        if(c==Retour)
        {
            Midlet.INSTANCE.display.setCurrent(new CarteList());
        }
        if(c==Noter)
        {
          //   Midlet.INSTANCE.disp.setCurrent(new Rating());
            
        }
        
    }
    
    
}
