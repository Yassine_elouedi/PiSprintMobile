/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import dao.CarteDAO;
import entities.Carte;
import tunisiamall.Midlet;

/**
 *
 * @author Yassine
 */
public class CarteList extends List implements CommandListener, Runnable {

    Command cmdDetail = new Command("Détail", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
   Carte[] Cartes;

    public CarteList(String title, int listType) {
        super(title, listType);
    }

    public CarteList() {
        super("Votre Carte", List.IMPLICIT);
        addCommand(cmdDetail);
        addCommand(cmdExit);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdDetail) {
           Midlet.INSTANCE.display.setCurrent(new DetailCarteForm(Cartes[getSelectedIndex()]));
        }
        if (c == cmdExit) {
           Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
    Cartes = new CarteDAO().select();
    
        
        if (Cartes.length > 0) {
            for (int i = 0; i < Cartes.length; i++) {
                append(Cartes[i].getNumero()+ " - " + Cartes[i].getNbrPt(), null);
            }
        }
    }
    
}
