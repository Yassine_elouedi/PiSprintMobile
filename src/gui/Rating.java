/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;


import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.*;
import entities.Promotion;
import tunisiamall.Midlet;


/**
 *
 * @author Chaabouni
 */
public class Rating extends Canvas implements CommandListener {
    int h = getHeight();
    int w = getWidth();
    private int numkey = 0;
    Promotion p = DetailForm.p;
    Command Back = new Command("retour", Command.SCREEN, 0);
    Command Noter = new Command("Confirmer", Command.SCREEN, 0);
    int intch;
    StringBuffer stb;
    public Rating() {
        addCommand(Noter);
        addCommand(Back);
        setCommandListener(this);
    }
    
    protected void paint(Graphics g) 
    {
        g.setColor(169,17,1);
        g.fillRect(0, 0, w, h);
        g.setColor(250,219,5);
        g.drawString("Evaluer le produit",60,120,Graphics.TOP | Graphics.LEFT);
        for(int i=65; i < 155;i+=18)
        {
            try {
                
                g.drawImage(Image.createImage("/ressources/Vide.png"), i, 160, Graphics.LEFT | Graphics.TOP);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        for(int i = 65;i<numkey*18+65;i+=18)
        {
            try {
                g.drawImage(Image.createImage("/ressources/Plein.png"), i, 160, Graphics.LEFT | Graphics.TOP);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    protected void keyPressed(int keyCode) {
        switch (keyCode) {
            case KEY_NUM1:
                numkey = 1;
                break;
            case KEY_NUM2:
                numkey = 2;
                break;
            case KEY_NUM3:
                numkey = 3;
                break;
            case KEY_NUM4:
                numkey = 4;
                break;
            case KEY_NUM5:
                numkey = 5;
                break;
        }
        switch (getGameAction(keyCode)) {
            case RIGHT:
                if(numkey < 5)
                    numkey++;
                break;
            case LEFT:
                if(numkey > 0)
                    numkey--;
                break;
        }
        repaint();
    }

    public void commandAction(Command c, Displayable d) {
        if(c == Noter)
        {            
            try {
                System.out.println(p.getId());
                HttpConnection hc = (HttpConnection)Connector.open("http://localhost/TunisiaMall22/update.php?id_user=40&id="+p.getId()+"&note="+numkey);
                DataInputStream dt = hc.openDataInputStream();
                stb = new StringBuffer();
                while((intch = dt.read()) != -1){
                    stb.append((char)intch);
                }
                System.out.println(stb.toString());
                if(stb.toString().equals("successfully added"))
                {
                    Alert a = new Alert("Opération réussit", "Vous avez noter a le produit "+p.getTauxdereduction(), null, AlertType.INFO);
                    a.setTimeout(7000);
                     Midlet.INSTANCE.display.setCurrent(a, new PromotionList());
                }
                else
                {
                    Alert a = new Alert("Not updated", "Erreur de mise a jour contacter votre administrateur", null, AlertType.ERROR);
                    a.setTimeout(7000);
                     Midlet.INSTANCE.display.setCurrent(a, new Rating());
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        if(c == Back)
        {
             Midlet.INSTANCE.display.setCurrent(new PromotionList());
        }
    }
}
