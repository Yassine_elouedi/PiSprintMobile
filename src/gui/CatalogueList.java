/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;



import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;

import javax.microedition.lcdui.List;
import dao.CatalogueDAO;
import tunisiamall.entities.Catalogue;
import tunisiamall.Midlet;

/**
 *
 * @author Yassine
 */
public class CatalogueList extends List implements CommandListener, Runnable {

    Command cmdDetail = new Command("Détail", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Catalogue[] Catalogues;

    public CatalogueList(String title, int listType) {
        super(title, listType);
    }

    public CatalogueList() {
        super("Liste des Catalogues", List.IMPLICIT);
        addCommand(cmdDetail);
        addCommand(cmdExit);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdDetail) {
           Midlet.INSTANCE.display.setCurrent(new DetailCatalogueForm(Catalogues[getSelectedIndex()]));
        }
        if (c == cmdExit) {
           Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
   Catalogues = new CatalogueDAO().select();
    
        
        if (Catalogues.length > 0) {
            for (int i = 0; i < Catalogues.length; i++) {
                append("Catalogue " +Catalogues[i].getLibelleEnseigne(), null);
            }
        }
    }
    
}
