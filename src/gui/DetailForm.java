/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.StringItem;
import entities.Promotion;
import tunisiamall.Midlet;

/**
 *
 * @author Asma
 */
public class DetailForm extends Form implements CommandListener{
    public static Promotion p;
    String url;
    StringBuffer stb;
    Image img;
    Image stars;
    int ch;
 
    StringItem image = new StringItem("Image", "");
    StringItem description = new StringItem("Description :","");
    StringItem taux = new StringItem("Taux de réduction:","");
    StringItem dateDebut = new StringItem("Date de début:","");
    StringItem dateFin = new StringItem("Date de fin:","");

    Command Noter = new Command("Noter promotion", Command.SCREEN, 0);
    Command Retour= new Command("Retour", Command.SCREEN, 0);

    public DetailForm(Promotion p) {
        
         super("Detail des promotions");
        this.p = p;
//              if(!"".equals(p.getImage()))
//        {
//            url = p.getImage();
//            try {
//                HttpConnection hc = (HttpConnection)Connector.open(url);
//                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
//                img = Image.createImage(dis);
//            } catch (IOException ex) {
//                 System.out.println(ex.getMessage());
//            }
//            append(img);
//        }
         
        
        taux.setText(String.valueOf(p.getTauxdereduction()));
        append(taux);
        dateDebut.setText(String.valueOf(p.getDateDebut()));
        append(dateDebut);
        dateFin.setText(String.valueOf(p.getDateDebut()));
        append(dateFin);
        description.setText(String.valueOf(p.getDescription()));
        append(description);
        if(!"".equals(p.getNote()));
        {
            try {
                int n=p.getNote().intValue();
           
                int x=n/4;
                HttpConnection hc = (HttpConnection)Connector.open("http://localhost/TunisiaMall22/Stars/"+String.valueOf(n)+"s.bmp");
                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                stars = Image.createImage(dis);
                append(stars);
            } catch (Exception ex) {
                 System.out.println(ex.getMessage());
            }
        }
       
        //image.setText(String.valueOf(p.getImage()));
        //append(image);
       // etat.setText(String.valueOf(p.getEtat()));
        //append(etat);
        //eval.setText(String.valueOf(p.getEval()));
        //append(eval);
        addCommand(Retour);
        addCommand(Noter);
        setCommandListener(this);
    }
   

    public void commandAction(Command c, Displayable d)
    {
        if(c==Retour)
        {
            Midlet.INSTANCE.display.setCurrent(new PromotionList());
        }
        if(c==Noter)
        {
             Midlet.INSTANCE.display.setCurrent(new Rating());
            
        }
        
    }
    
    
}
