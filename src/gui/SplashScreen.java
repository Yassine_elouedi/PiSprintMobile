/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import tunisiamall.Midlet;
import tunisiamall.MyCanvas;

/**
 *
 * @author bof
 */
public class SplashScreen extends Canvas implements Runnable
{
     private Image mImage;

    /**
     * The constructor attempts to load the named image and begins a timeout
     * thread. The splash screen can be dismissed with a key press,
     * a pointer press, or a timeout
     * @param projectMIDlet instance of MIDlet
     */
    public SplashScreen(){
     
        try{
        mImage = Image.createImage("/tunisiamall/splash.png");
        Thread t = new Thread(this);
        t.start();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    /**
     * Paints the image centered on the screen.
     */
    public void paint(Graphics g) {
        int width = getWidth();
        int height = getHeight();
        //set background color to overdraw what ever was previously displayed

        
        g.setColor(0x000000);
        g.fillRect(0,0, width, height);
        g.drawImage(mImage, 0, 0,
                Graphics.TOP | Graphics.LEFT);
    }
    /**
     * Dismisses the splash screen with a key press or a pointer press
     */
    public void dismiss() {
        if (isShown())
            Midlet.INSTANCE.display.setCurrent(new MyCanvas());
                    

    }
    /**
     * Default timeout with thread
     */
    public void run() {
        try {
            Thread.sleep(5000);//set for 3 seconds
        }
        catch (InterruptedException e) {
            System.out.println("InterruptedException");
            e.printStackTrace();
        }
        dismiss();
    }
    
}
