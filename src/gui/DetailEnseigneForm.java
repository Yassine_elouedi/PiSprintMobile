/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.StringItem;
import entities.Enseigne;
import tunisiamall.Midlet;


/**
 *
 * @author Yassine
 */
public class DetailEnseigneForm extends Form implements CommandListener{
    public static Enseigne p;
    String url;
    StringBuffer stb;
    Image img;
    Image stars;
    int ch;
 
    StringItem image = new StringItem("Image", "");
    StringItem description = new StringItem("Description :","");
    StringItem libelle = new StringItem("Libelle:","");
    StringItem TimeOuveture = new StringItem("Heure d'ouverture:","");
    

    Command Noter = new Command("Noter enseigne", Command.SCREEN, 0);
    Command Retour= new Command("Retour", Command.SCREEN, 0);

    public DetailEnseigneForm(Enseigne p) {
        
         super("Detail enseigne");
        this.p = p;
//              if(!"".equals(p.getImage()))
//        {
//            url = p.getImage();
//            try {
//                HttpConnection hc = (HttpConnection)Connector.open(url);
//                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
//                img = Image.createImage(dis);
//            } catch (IOException ex) {
//                 System.out.println(ex.getMessage());
//            }
//            append(img);
//        }
         
        
        libelle.setText(String.valueOf(p.getLibelle()));
        append(libelle);
        description.setText(String.valueOf(p.getDescription()));
        append(description);
        TimeOuveture.setText(String.valueOf(p.getTimeOuverture()));
        append(TimeOuveture);
       
//        if(!"".equals(p.getNote()));
//        {
//            try {
//                int n=p.getNote().intValue();
//           
//                int x=n/4;
//                HttpConnection hc = (HttpConnection)Connector.open("http://localhost/TunisiaMall22/Stars/"+String.valueOf(n)+"s.bmp");
//                DataInputStream dis = new DataInputStream(hc.openDataInputStream());
//                stars = Image.createImage(dis);
//                append(stars);
//            } catch (Exception ex) {
//                 System.out.println(ex.getMessage());
//            }
//        }
       
        //image.setText(String.valueOf(p.getImage()));
        //append(image);
       // etat.setText(String.valueOf(p.getEtat()));
        //append(etat);
        //eval.setText(String.valueOf(p.getEval()));
        //append(eval);
        addCommand(Retour);
        addCommand(Noter);
        setCommandListener(this);
    }
   

    public void commandAction(Command c, Displayable d)
    {
        if(c==Retour)
        {
            Midlet.INSTANCE.display.setCurrent(new EnseigneList());
        }
        if(c==Noter)
        {
          //   Midlet.INSTANCE.disp.setCurrent(new Rating());
            
        }
        
    }
    
    
}
