/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.microedition.midlet.MIDlet;
import dao.ProduitDAO;
import entities.Produit;
import tunisiamall.Midlet;


/**
 *
 * @author ISLEM
 */
public class ProduitList extends List implements CommandListener, Runnable {

    Command cmdDetail = new Command("Détail", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmdback = new Command("Back", Command.BACK, 0);
    Produit[] Produits;
    List lst = new List("Liste", List.IMPLICIT);
    
    
    
    public ProduitList() {
        
        
        super("Liste des produits", List.IMPLICIT);
        addCommand(cmdDetail);
        addCommand(cmdExit);
        addCommand(cmdback);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }
    
    public ProduitList(String title, int listType) {
        super(title, listType);
    }

    public void commandAction(Command c, Displayable d) {
       if (c == cmdDetail) {
           Midlet.INSTANCE.display.setCurrent(new DetailProduitForm(Produits[getSelectedIndex()].getDescriptif()));
        }
       if (c == cmdback) {
            Midlet.INSTANCE.display.setCurrent(lst);
        }
        if (c == cmdExit) {
           Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
       Produits = new ProduitDAO().select();
    
        
        if (Produits.length > 0) {
            for (int i = 0; i < Produits.length; i++) {
                append("Produit : "+Produits[i].getLibelle(), null);
                //append("Descriptif" + Produits[i].getDescriptif(), null);
                append("Cat�gorie : " + Produits[i].getCategorie(), null);
//                append("Prix : " + Produits[i].getPrix(), null);
            }
        }
    }
    }
    
