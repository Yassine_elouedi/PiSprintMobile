/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import dao.PromotionDAO;
import entities.Promotion;
import tunisiamall.Midlet;

/**
 *
 * @author Asma
 */
public class PromotionList extends List implements CommandListener, Runnable {

    Command cmdDetail = new Command("Détail", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Promotion[] promotions;

    public PromotionList(String title, int listType) {
        super(title, listType);
    }

    public PromotionList() {
        super("Liste Promotion", List.IMPLICIT);
        addCommand(cmdDetail);
        addCommand(cmdExit);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdDetail) {
            Midlet.INSTANCE.display.setCurrent(new DetailForm(promotions[getSelectedIndex()]));
        }
        if (c == cmdExit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
     promotions = new PromotionDAO().select();
        if (promotions.length > 0) {
            for (int i = 0; i < promotions.length; i++) {
                append(promotions[i].getTauxdereduction()+ " - " + promotions[i].getDateDebut(), null);
            }
        }
    }

}
