/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import dao.EnseigneDAO;
import entities.Enseigne;
import tunisiamall.Midlet;

/**
 *
 * @author Yassine
 */
public class EnseigneList extends List implements CommandListener, Runnable {

    Command cmdDetail = new Command("Détail", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
   Enseigne[] Enseignes;

    public EnseigneList(String title, int listType) {
        super(title, listType);
    }

    public EnseigneList() {
        super("Liste des enseignes", List.IMPLICIT);
        addCommand(cmdDetail);
        addCommand(cmdExit);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdDetail) {
           Midlet.INSTANCE.display.setCurrent(new DetailEnseigneForm(Enseignes[getSelectedIndex()]));
        }
        if (c == cmdExit) {
           Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
    Enseignes = new EnseigneDAO().select();
    
        
        if (Enseignes.length > 0) {
            for (int i = 0; i < Enseignes.length; i++) {
                append(Enseignes[i].getLibelle()+ " - " + Enseignes[i].getDescription(), null);
            }
        }
    }
    
}
