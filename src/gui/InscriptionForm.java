/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dao.UserDAO;
import entities.User;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import tunisiamall.Midlet;
import tunisiamall.MyCanvas;

/**
 *
 * @author bof
 */
public class InscriptionForm extends Form implements CommandListener, Runnable {
     TextField tLogIns = new TextField("login","",50, TextField.ANY);
    TextField tEmailIns = new TextField("email","",50, TextField.ANY);
    TextField tPasswordIns = new TextField("password","",50, TextField.PASSWORD);
    TextField tPasswordConfIns = new TextField("confirm","",50, TextField.PASSWORD);
        Command cmdInscription = new Command("Inscription",Command.OK,0);
        Command cmdBack = new Command ("Back",Command.BACK,1);

    public InscriptionForm() {
        super("Inscription");
        append(tLogIns);
       append(tEmailIns);
        append(tPasswordIns);
       append(tPasswordConfIns);
       addCommand(cmdInscription);
        addCommand(cmdBack);
        setCommandListener(this);
               
    }

    public void commandAction(Command c, Displayable d) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    if(c==cmdInscription){
        Thread th = new Thread(this);
            th.start();
    }
    if(c==cmdBack){
        Midlet.INSTANCE.display.setCurrent(new MyCanvas());
    }
    }

    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       String email = tEmailIns.getString();
        String log = tLogIns.getString();
        String pass = tPasswordIns.getString();
        boolean result = new UserDAO().insert(new User(log, pass, email));
        Alert alert = new Alert("R�sultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Inscription compl�t�e avec succ�s");
            Midlet.INSTANCE.display.setCurrent(alert);
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Erreur lors de l'inscription");
            Midlet.INSTANCE.display.setCurrent(alert);
    
    }
    }
    
}
